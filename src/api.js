import axios from 'axios';

axios.defaults.baseURL = process.env.VUE_APP_API_URI;

const getData = e => e.data;

const api = {
    images:{
        get:(page)=>
            axios.get(`/images/${page}`).then(getData),
        getClouds:()=>
            axios.get(`/clouds`).then(getData),
        add:(data)=>
            axios.post(`/clouds`,data).then(getData)
    },
    response:{
        add:(data)=>
            axios.post(`/responses`, data).then(getData)
    }
}

export default api;