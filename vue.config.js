module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  pwa: {
    themeColor: "#130f40",
    msTileColor: "#130f40",
    name: "What you see in the Cloud",
  }
}
